package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String roman) {
        Integer output = 0;
        Map<Character, Integer> romanToInteger = new HashMap<>();
        romanToInteger.put('I',1);
        romanToInteger.put('V',5);
        romanToInteger.put('X',10);
        romanToInteger.put('L',50);
        romanToInteger.put('C',100);
        romanToInteger.put('D',500);
        romanToInteger.put('M',1000);
        if(romanToInteger.containsKey(roman)){
            output = romanToInteger.get(roman);
        }else {
            boolean valid = roman.matches("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");
            if(valid == false) throw new IllegalArgumentException("Invalid Roman Value has passed");
            for (int i = 0; i < roman.length(); i++) {
                int s1 = romanToInteger.get(roman.charAt(i));
                if (i + 1 < roman.length()) {
                    int s2 = romanToInteger.get(roman.charAt(i + 1));
                    if (s1 >= s2) {
                        output = output + s1;
                    } else {
                        output = output + s2 - s1;
                        i++;
                    }
                } else {
                    output = output + s1;
                }
            }
        }
        if (output <= 0) {
            return -1;
        }
        return output;
    }
}
