package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class RomanConverterTest {
    private static final Map<String, Integer> INPUT_AND_EXPECTED_VALUES_MAP = new HashMap<String, Integer>() {{
        put("MMXII", 2012);
        put("XXXVI", 36);
        put("MCMXCVI", 1996);
    }};

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @ParameterizedTest
    @ValueSource(strings = { "MMXII","XXXVI", "MCMXCVI"})
    void testWithValueSource(String argument) {
        assertEquals(INPUT_AND_EXPECTED_VALUES_MAP.get(argument), romanConvertor.convertRomanToArabicNumber(argument));
    }

    @Test()
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            romanConvertor.convertRomanToArabicNumber("IIII");
        });
    }
}